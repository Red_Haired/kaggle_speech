import numpy as np
from scipy.io import wavfile
# Data Loading
import os
import re
from glob import glob
import numbers
import tensorflow as tf
import librosa
Dataset = tf.data.Dataset

POSSIBLE_LABELS = 'yes no up down left right on off stop go silence unknown'.split()
id2name = {i: name for i, name in enumerate(POSSIBLE_LABELS)}
name2id = {name: i for i, name in id2name.items()}
SOUND_LENGTH = 16000


class SkipSilence(Exception):
    pass


def classes_probabilities(**kwargs):
    p = np.zeros(len(POSSIBLE_LABELS))
    s = 0.0
    for label, prob in kwargs.items():
        s += prob
        p[name2id[label]] = prob
    remaining = 1.0 - s
    if remaining < 0.0:
        raise ValueError("Sum of probabilities should be less than 1.0")
    rem_prob = remaining / (len(POSSIBLE_LABELS) - len(kwargs))
    for i in range(len(p)):
        if id2name[i] in kwargs:
            continue
        p[i] = rem_prob
    return p


def load_filenames(data_dir):
    """ Return 2 lists of filenames: train, val
    """
    # Just a simple regexp for paths with three groups:
    # prefix, label, user_id
    pattern = re.compile("(.+\/)?(\w+)\/([^_]+)_.+wav")
    all_files = glob(os.path.join(data_dir, 'train/audio/*/*wav'))

    with open(os.path.join(data_dir, 'train/validation_list.txt'), 'r') as fin:
        validation_files = fin.readlines()
    valset = set()
    for entry in validation_files:
        r = re.match(pattern, entry)
        if r:
            valset.add(r.group(3))

    train, val = [], []
    for entry in all_files:
        r = re.match(pattern, entry)
        if r:
            label, uid = r.group(2), r.group(3)
            sample = entry
            if uid in valset:
                val.append(sample)
            else:
                train.append(sample)

    print('There are {} train and {} val samples'.format(len(train), len(val)))
    return train, val


class DataGenerator(object):
    def __init__(self, filenames):
        self.possible_labels = set(POSSIBLE_LABELS)
        self.filenames_per_label = [[] for _ in POSSIBLE_LABELS]

        for filename in filenames:
            label = self.get_label(filename)
            label_id = name2id[label]
            self.filenames_per_label[label_id].append(filename)

        self.filenames = filenames
        self.tf_datasets = []
        self.silence_cache = []
        self.silence_probs = []
        self.silence_scales = [1.0, 0.1, 0.01, 0.001, 0.0001, 0.00001, 0.000001, 0]
        self.silence_scale_probs = [0.1, 0.1, 0.4, 0.1, 0.1, 0.1, 0.09, 0.01]
        self.classes_probs = classes_probabilities()
        self.load_silence()

    def load_silence(self):
        self.silence_cache = []
        if self.filenames_per_label[name2id['silence']]:
            silence_files = self.filenames_per_label[name2id['silence']]
            self.silence_mix = 1
        else:
            silence_files = glob(os.path.join('data', 'train/audio/_background_noise_/*wav'))
            self.silence_mix = 2
        self.silence_probs = []
        long_file_prob = 60.0
        for filename in silence_files:
            wav = self.read_wav(filename)
            scale = 1.0
            filename = os.path.basename(filename)
            p = 1.0
            if filename in ['pink_noise.wav', 'white_noise.wav']:
                scale = 0.0001
                p = long_file_prob
            elif filename == 'running_tap.wav':
                scale = 0.001
                p = long_file_prob
            elif filename == 'exercise_bike.wav' or filename == 'doing_the_dishes.wav':
                scale = 0.1
                p = long_file_prob
            elif filename == 'dude_miaowing.wav':
                p = long_file_prob
            self.silence_cache.append(wav * scale)
            self.silence_probs.append(p)
        self.silence_probs /= np.sum(self.silence_probs)

    def generate_silence(self):
        if not self.silence_cache:
            raise SkipSilence
        wavs = []
        scales = []
        for _ in range(self.silence_mix):
            wav = np.random.choice(self.silence_cache, p=self.silence_probs)
            wav = self.fit_length(wav)
            wavs.append(wav)
            scale = np.random.random()
            scales.append(scale)
        scales /= np.sum(scales)
        wav = np.dot(scales, wavs)
        scale = np.random.random()**2
        scale = np.random.choice([scale, 0.0], p=[0.8, 0.2])
        return self.fit_length(wav*scale)

    def get_label(self, filename):
        if not hasattr(self, 'pattern'):
            self.pattern = re.compile("(.+\/)?(\w+)\/([^_]+)_.+wav")
        r = re.match(self.pattern, filename)
        label = r.group(2)
        if label == '_background_noise_':
            label = 'silence'
        if label not in self.possible_labels:
            label = 'unknown'
        return label

    def random_sample(self, label=None, augment=True, correct_distribution=True):

        if label is None:
            if correct_distribution:
                label = np.random.choice(POSSIBLE_LABELS, p=self.classes_probs)
                label = name2id[label]
                if label != name2id['silence']:
                    filename = np.random.choice(self.filenames_per_label[label])
            else:
                filename = np.random.choice(self.filenames)
                label = name2id[self.get_label(filename)]
        else:
            if not isinstance(label, numbers.Integral):
                label = name2id[label]
            if label != name2id['silence']:
                filename = np.random.choice(self.filenames_per_label[label])

        if label == name2id['silence']:
            wav = self.generate_silence()
            add_noise = False
        else:
            wav = self.read_wav(filename)
            add_noise = True
        if augment:
            wav = self.augment_wav(wav, add_noise=add_noise, shift_time=True, time_stretch=True)
        wav = DataGenerator.fit_length(wav)
        return np.int32(label), wav

    def get_sample(self, index, include_label=True, include_filename=False):
        filename = self.filenames[index]
        wav = self.read_wav(filename)
        wav = DataGenerator.fit_length(wav)
        if include_label:
            label = self.get_label(filename)
            label = np.int32(name2id[label])
            if include_filename:
                return label, filename, wav
            return label, wav
        elif include_filename:
            filename = os.path.basename(filename)
            return filename, wav
        else:
            return wav

    def augment_wav(self, wav, add_noise=True, shift_time=True, time_stretch=True):
        scale = 1.4 * np.random.random() + 0.1
        wav *= scale
        if time_stretch:
            if np.random.randint(2):
                stretch_rate = 0.2*np.random.random() + 0.9
                wav = librosa.effects.time_stretch(wav, stretch_rate)
        if shift_time:
            start_ = int(np.random.uniform(-4800, 4800))
            if start_ >= 0:
                wav = np.r_[wav[start_:], np.random.uniform(-0.001, 0.001, start_)]
            else:
                wav = np.r_[np.random.uniform(-0.001, 0.001, -start_), wav[:start_]]
        if add_noise:
            noise = self.generate_silence()
            noise_scale = np.random.choice([1.0, 0.0])
            wav = DataGenerator.fit_length(wav)
            wav += noise * noise_scale
        return wav


    @staticmethod
    def fit_length(wav):
        L = SOUND_LENGTH  # be aware, some files are shorter than 1 sec!
        if len(wav) < L:
            start_ = (L - len(wav))//2
            wav = np.r_[np.random.uniform(-0.001, 0.001, start_), wav, np.random.uniform(-0.001, 0.001, L - len(wav) - start_)]
        if len(wav) > L:
            beg = np.random.randint(0, len(wav) - L)
        else:
            beg = 0
        return wav[beg: beg + L]

    @staticmethod
    def read_wav(fname):
        _, wav = wavfile.read(fname)
        wav = wav.astype(np.float32) / np.iinfo(np.int16).max
        s = np.std(wav)
        if s > 0.0:
            wav /= s
        return wav

    def get_random_generator(self, correct_distribution=True, augment=True, repeate_sample=False):
        def random_generator():
            while True:
                try:
                    sample = self.random_sample(correct_distribution=correct_distribution, augment=augment)
                    if not repeate_sample:
                        yield sample
                    else:
                        wav = sample[-1]
                        wav1 = np.pad(wav, 1000, 'constant')
                        wav = (wav, wav1[len(wav1) - SOUND_LENGTH:])
                        for w in wav:
                            s = *sample[:-1], w
                            yield s
                except SkipSilence:
                    pass
                except Exception as err:
                    print("Error generating sample", err)
        return random_generator

    def get_generator(self, include_label=True, include_filename=False, repeate_sample=False):
        def generator():
            for i in range(len(self.filenames)):
                try:
                    sample = self.get_sample(i, include_label=include_label, include_filename=include_filename)
                    if not repeate_sample:
                        yield sample
                    else:
                        wav = sample[-1]
                        wav1 = np.pad(wav, 1000, 'constant')
                        wav = (wav, wav1[len(wav1) - SOUND_LENGTH:])
                        for w in wav:
                            s = *sample[:-1], w
                            yield s
                except Exception as err:
                    print(err, self.filenames[i])
        return generator

    def get_tf_iterator(self, batch_size=64, random=False, repeat=False, augment=True, repeate_sample=False):
        n = 1
        if repeate_sample:
            n = 2
        ds_types = (tf.int32, tf.float32)
        ds_shapes = ([], [SOUND_LENGTH])
        if random:
            generator = self.get_random_generator(augment=augment, repeate_sample=repeate_sample)
        else:
            generator = self.get_generator(repeate_sample=repeate_sample)

        data = Dataset.from_generator(generator, ds_types, ds_shapes)
        if repeat:
            data = data.repeat()
        else:
            data = data.take(len(self.filenames)*n)
        data = data.batch(batch_size=batch_size*n)
        data = data.prefetch(20)
        self.tf_datasets.append(data)
        return data.make_initializable_iterator()



