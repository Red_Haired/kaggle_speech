from datagenerator import DataGenerator, load_filenames, POSSIBLE_LABELS, SOUND_LENGTH, name2id, id2name
from manual_train import DATADIR
import pytest
import numpy as np
import os
import tensorflow as tf

@pytest.fixture
def train_datagen():
    train, val = load_filenames(DATADIR)
    return DataGenerator(train)

@pytest.fixture
def simple_datagen():
    filenames = [
        'yes/0f7dc557_nohash_0.wav',
        'no/0f7dc557_nohash_1.wav',
        '_background_noise_/doing_the_dishes.wav',
        'zero/0bde966a_nohash_1.wav',
    ]
    filenames = [os.path.join(DATADIR, 'train/audio', f) for f in filenames]
    return DataGenerator(filenames)


def test_get_sample(train_datagen):
    label, wav = train_datagen.get_sample(0)
    assert type(label) == np.int32
    assert 0 <= label <= len(POSSIBLE_LABELS)
    assert np.shape(wav) == (SOUND_LENGTH,)


def test_get_generator(train_datagen):
    generator = train_datagen.get_generator()

    for label, wav in generator():
        assert type(label) == np.int32
        assert 0 <= label <= len(POSSIBLE_LABELS)
        assert np.shape(wav) == (SOUND_LENGTH,)


def test_get_random_generator(train_datagen):
    generator = train_datagen.get_random_generator()

    for label, wav in generator():
        assert type(label) == np.int32
        assert 0 <= label <= len(POSSIBLE_LABELS)
        assert np.shape(wav) == (SOUND_LENGTH,)

def test_simple_get_generator(simple_datagen):
    generator = simple_datagen.get_generator()()
    label, wav = next(generator)
    assert label == name2id['yes']
    label, wav = next(generator)
    assert label == name2id['no']
    label, wav = next(generator)
    assert label == name2id['silence']
    label, wav = next(generator)
    assert label == name2id['unknown']

    generator = simple_datagen.get_generator(repeate_sample=True)()
    label1, wav1 = next(generator)
    label2, wav2 = next(generator)
    assert label1 == label2

    generator = simple_datagen.get_generator(include_label=False, include_filename=True, repeate_sample=True)
    for fname, wav_val in generator():
        print(fname, np.shape(wav_val))


def test_tf_iterator(simple_datagen):
    batch_size = 3
    iterator = simple_datagen.get_tf_iterator(batch_size=batch_size, repeate_sample=True)
    labels, wav = iterator.get_next()
    sess = tf.Session()
    sess.run(iterator.initializer)
    labels_val, wav_val = sess.run((labels, wav))
    assert np.shape(labels_val) == (batch_size*2,)
    assert np.shape(wav_val) == (batch_size*2, SOUND_LENGTH)
    labels_val = np.reshape(labels_val, (-1, 2))
    assert np.array_equal(labels_val[:, 0], labels_val[:, 1])
    labels_val, wav_val = sess.run((labels, wav))



