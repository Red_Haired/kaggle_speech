import tensorflow as tf
from scipy.io import wavfile
import numpy as np
from datagenerator import id2name

if __name__ == '__main__':
    pass
    saver = tf.train.import_meta_graph('/home/red-haired/programming/kaggle_speech/kaggle_speach_baseline_model.meta')
    graph = tf.get_default_graph()
    prediction = graph.get_tensor_by_name('prediction:0')
    wav = graph.get_tensor_by_name('wav:0')
    path = '/home/red-haired/programming/kaggle_speech/data/train/audio/no/0d2bcf9d_nohash_0.wav'

    _, wav_val = wavfile.read(path)
    wav_val = wav_val.astype(np.float32) / np.iinfo(np.int16).max

    with tf.Session() as sess:
        # To initialize values with saved data
        saver.restore(sess, tf.train.latest_checkpoint('./'))
        res = sess.run(prediction, feed_dict={wav:[wav_val]})
        print(res[0], id2name[res[0]])
