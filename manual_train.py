import os
import tensorflow as tf
from datagenerator import DataGenerator, load_filenames, POSSIBLE_LABELS, name2id
from baseline import baseline
from rnn import rnn
from resnet import resnet
import numpy as np
import argparse
from tensorflow.python import debug as tf_debug
Dataset = tf.data.Dataset

DATADIR = './data' # unzipped train and test data
OUTDIR = './model-' # just a random name


params = dict(
    seed=2018,
    batch_size=64,
    keep_prob=0.5,
    learning_rate=1e-4,
    clip_gradients=15.0,
    use_batch_norm=True,
    num_classes=len(POSSIBLE_LABELS),
)


def load_data(data_dir, repeat=True, ensemble=False):
    trainfiles, valfiles = load_filenames(data_dir)
    # setup training and validation datasets
    train_data_generator = DataGenerator(trainfiles)
    val_data_generator = DataGenerator(valfiles)
    training_iterator = train_data_generator.get_tf_iterator(batch_size=params['batch_size'], random=repeat, repeat=repeat, repeate_sample=ensemble)
    validation_iterator = val_data_generator.get_tf_iterator(batch_size=params['batch_size'], random=repeat, repeat=repeat, augment=False, repeate_sample=ensemble)
    return training_iterator, validation_iterator


def model_loss(labels, logits, name, l2_reg=0.00001):
    loss = tf.reduce_mean(
        tf.nn.sparse_softmax_cross_entropy_with_logits(labels=labels, logits=logits), name=name)
    l2 = l2_reg * sum(
        tf.nn.l2_loss(tf_var)
        for tf_var in tf.trainable_variables()
        if 'weights' in tf_var.name
        #if (not ("noreg" in tf_var.name or "bias" in tf_var.name) and 'bidirectional_rnn' in tf_var.name)
    )
    return loss + l2


def set_iterator(sess, **kwargs):
    # feedable iterator magic
    # choose which of iterators to use with setting iter_mode in sess.run()
    iter_mode = tf.placeholder(tf.string, shape=[])
    iterator = tf.data.Iterator.from_string_handle(
        iter_mode, kwargs['train'].output_types, kwargs['train'].output_shapes)
    modes = {}
    for mode, dataset in kwargs.iteritems():
        custom_iterator = dataset.make_initializable_iterator()
        modes[mode] = sess.run(custom_iterator.string_handle())
    return iterator, iter_mode, modes


def ensemble_prediction(labels, logits, n=2):
    reduced_labels = tf.reshape(labels, [-1, 2])[:, 0]
    reduced_logits = tf.reduce_mean(tf.reshape(logits, [-1, 2, params['num_classes']]), axis=1)
    reduced_prediction = tf.cast(tf.argmax(reduced_logits, axis=-1), tf.int32)
    equality = tf.equal(reduced_prediction, reduced_labels)
    accuracy = tf.reduce_mean(tf.cast(equality, tf.float32))
    return reduced_prediction, accuracy, reduced_labels


def train(extractor_fn, model_dir, debug=False, resume=False):
    os.makedirs(model_dir, exist_ok=True)

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)
    #sess = tf_debug.LocalCLIDebugWrapperSession(sess)
    is_train = tf.placeholder_with_default(False, shape=(), name='is_train')
    learning_rate = tf.placeholder_with_default(params['learning_rate'], shape=(), name='learning_rate')
    training_iterator, validation_iterator = load_data(DATADIR)

    # feedable iterator magic
    # choose which of iterators to use with setting iter_mode in sess.run()
    iter_mode = tf.placeholder(tf.string, shape=[], name='iter_mode')
    iterator = tf.data.Iterator.from_string_handle(
                       iter_mode,
                       training_iterator.output_types,
                       training_iterator.output_shapes)
    training_mode = sess.run(training_iterator.string_handle())
    validation_mode = sess.run(validation_iterator.string_handle())

    labels, wav = iterator.get_next()
    wav = tf.identity(wav, name='wav')
    labels = tf.identity(labels, name='labels')
    logits = extractor_fn(wav, params, is_train)
    logits = tf.identity(logits, name='logits')
    prediction = tf.cast(tf.argmax(logits, axis=-1), tf.int32, name='prediction')

    equality = tf.equal(prediction, labels)
    accuracy = tf.reduce_mean(tf.cast(equality, tf.float32), name='accuracy')
    tf.summary.scalar('accuracy', accuracy)

    #percentile = tf.contrib.distributions.percentile(tf.abs(wav), q=99, axis=[1])
    #quiet = tf.less(percentile, 0.001)
    #quietness = tf.reduce_mean(tf.cast(quiet, tf.float32))
    #tf.summary.scalar('quiteness', quietness)
    #prediction = tf.where(quiet, tf.ones_like(prediction)*name2id['silence'], prediction, name='prediction')

    #equality = tf.equal(prediction, labels)
    #accuracy = tf.reduce_mean(tf.cast(equality, tf.float32), name='accuracy_s')
    loss = model_loss(labels=labels, logits=logits, name='loss')

    #tf.summary.scalar('accuracy_s', accuracy)
    tf.summary.scalar('loss', loss)

    # to update internal state variable of batch normalization
    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    with tf.control_dependencies(update_ops):
        train_step = tf.train.AdamOptimizer(learning_rate).minimize(loss)

    merged_summary = tf.summary.merge_all()
    train_writer = tf.summary.FileWriter(model_dir + '/train', sess.graph)
    test_writer = tf.summary.FileWriter(model_dir + '/test')

    saver = tf.train.Saver()
    sess.run(tf.global_variables_initializer())
    sess.run(validation_iterator.initializer)
    sess.run(training_iterator.initializer)
    if not resume:
        saver.save(sess, os.path.join(model_dir, 'model'))
    best_accuracy = 0.0
    acc_average = 0.0
    alpha = 0.01
    if debug:
        run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
        run_metadata = tf.RunMetadata()
    else:
        run_options = None
        run_metadata = None

    max_iteration = 100001
    learning_rate_value = params['learning_rate']

    start_iteration = 0
    if resume:
        chekpoint_filepath = tf.train.latest_checkpoint(model_dir)
        saver.restore(sess, chekpoint_filepath)
        start_iteration = int(chekpoint_filepath.split('-')[-1])
    for i in range(start_iteration, max_iteration):
        summary, _ = sess.run((merged_summary, train_step), feed_dict={iter_mode: training_mode, is_train: True})
        train_writer.add_summary(summary, i)
        if debug:
            train_writer.add_run_metadata(run_metadata, 'step%d' % i)
        if i % 5 == 0:
            summary, loss_val, acc_val = sess.run((merged_summary, loss, accuracy),
                                                  options=run_options,
                                                  run_metadata=run_metadata,
                                                  feed_dict={iter_mode: validation_mode, learning_rate: learning_rate_value})
            print(i, loss_val, acc_val)
            if acc_average == 0.0:
                acc_average = acc_val
            else:
                acc_average = alpha*acc_val + (1.0 - alpha)*acc_average
            test_writer.add_summary(summary, i)
            if debug:
                test_writer.add_run_metadata(run_metadata, 'step%d' % i)
            test_writer.flush()
            train_writer.flush()
        if i % 1000 == 0:
            if acc_average >= best_accuracy:
                best_accuracy = acc_average
                print('creating checkpoint with accuracy ', acc_average, os.path.join(model_dir, 'model-') + str(i))
                saver.save(sess, os.path.join(model_dir, 'model'), global_step=i)
        if i > 20000 and i % 10001 == 0:
            learning_rate_value /= 10.0
    saver.save(sess, os.path.join(model_dir, 'model'), global_step=max_iteration)
    pass


if __name__ == "__main__":
    extractor_fn = resnet
    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--model_dir', type=str, default=OUTDIR + extractor_fn.__name__)
    parser.add_argument('--resume', action='store_true')
    args = parser.parse_args()
    train(extractor_fn, model_dir=args.model_dir, debug=args.debug, resume=args.resume)