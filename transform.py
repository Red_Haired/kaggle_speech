import numpy as np
import tensorflow as tf
from tensorflow.contrib import signal


def spectrogram(wav, name=None):
    specgram = signal.stft(
        wav,
        400,  # 16000 [samples per second] * 0.025 [s] -- default stft window frame
        100,  # 16000 * 0.010 -- default stride
    )
    eps = tf.constant(1e-10)
    # log(1 + abs) is a default transformation for energy units
    amp = tf.log(tf.abs(specgram) + eps)

    x = tf.stack([amp], axis=3)  # shape is [bs, time, freq_bins, 1]
    x = tf.to_float(x)  # we want to have float32, not float64
    x = tf.identity(x, name=name)
    return x
