import tensorflow as tf
slim= tf.contrib.slim
from transform import spectrogram
from inception_resnet_v2 import *


def resnet(wav, params, is_training):
    x = spectrogram(wav, name='spectrogram')
    with slim.arg_scope(inception_resnet_v2_arg_scope()):
        logits, _ = inception_resnet_v2(x, num_classes=params['num_classes'], is_training=is_training)
    return logits
