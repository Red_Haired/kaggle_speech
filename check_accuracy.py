import tensorflow as tf
from scipy.io import wavfile
import numpy as np
from glob import glob
import os
import argparse
import matplotlib.pyplot as plt
import itertools
from datagenerator import id2name, DataGenerator, POSSIBLE_LABELS
from manual_train import OUTDIR, DATADIR, params, load_data, ensemble_prediction
Dataset = tf.data.Dataset
confusion_matrix = tf.contrib.metrics.confusion_matrix


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_dir', type=str, default=OUTDIR+'rnn')
    parser.add_argument('--iteration', default=None)
    parser.add_argument('--ensemble', action='store_true')
    parser.add_argument('--batch_size', type=int, default=params['batch_size'])
    args = parser.parse_args()
    params['batch_size'] = args.batch_size

    model_meta_path = tf.train.latest_checkpoint(args.model_dir) + '.meta'
    saver = tf.train.import_meta_graph(model_meta_path)
    graph = tf.get_default_graph()

    training_iterator, validation_iterator = load_data(DATADIR, repeat=False, ensemble=args.ensemble)

    test_sample = glob(os.path.join(DATADIR, 'test_sample/*/*wav'))
    sample_generator = DataGenerator(test_sample)
    sample_iterator = sample_generator.get_tf_iterator(batch_size=params['batch_size'], repeate_sample=args.ensemble)

    accuracy = graph.get_tensor_by_name('accuracy:0')
    loss = graph.get_tensor_by_name('loss:0')
    wav = graph.get_tensor_by_name('wav:0')
    labels = graph.get_tensor_by_name('labels:0')
    logits = graph.get_tensor_by_name('logits:0')
    prediction = graph.get_tensor_by_name('prediction:0')
    iter_mode = graph.get_tensor_by_name('iter_mode:0')
    if args.ensemble:
        prediction, accuracy, labels = ensemble_prediction(labels, logits)

    confusion = confusion_matrix(labels, prediction, num_classes=len(POSSIBLE_LABELS))
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True

    with tf.Session(config=config) as sess:
        training_mode = sess.run(training_iterator.string_handle())
        validation_mode = sess.run(validation_iterator.string_handle())
        sample_mode = sess.run(sample_iterator.string_handle())
        # To initialize values with saved data
        if not args.iteration:
            chekpoint_filepath = tf.train.latest_checkpoint(args.model_dir)
        else:
            name = 'model-%d' % int(args.iteration)
            chekpoint_filepath = os.path.join(args.model_dir, name)
        saver.restore(sess, chekpoint_filepath)
        sess.run(validation_iterator.initializer)
        sess.run(training_iterator.initializer)
        sess.run(sample_iterator.initializer)
        full_accuracies = []
        full_losses = []
        full_confusions = []
        for mode in [training_mode, validation_mode, sample_mode]:
            accuracies = []
            losses = []
            confusions = []
            while True:
                try:
                    accuracy_val, loss_val, confusion_val = sess.run((accuracy, loss, confusion), feed_dict={iter_mode: mode})
                    accuracies.append(accuracy_val)
                    losses.append(loss_val)
                    confusions.append(confusion_val)
                except tf.errors.OutOfRangeError:
                    break
            full_accuracies.append(np.mean(accuracies))
            full_losses.append(np.mean(losses))
            full_confusions.append(np.sum(confusions, axis=0))

        print('Training loss:       ', full_losses[0])
        print('Validation loss:     ', full_losses[1])
        print('Test sample loss:     ', full_losses[2])
        print('Training accuracy:   ', full_accuracies[0])
        print('Validation accuracy: ', full_accuracies[1])
        print('Test sample accuracy: ', full_accuracies[2])
        plt.figure()
        plot_confusion_matrix(full_confusions[0], classes=POSSIBLE_LABELS, normalize=True,
                      title='Training confusion matrix')
        plt.figure()
        plot_confusion_matrix(full_confusions[1], classes=POSSIBLE_LABELS, normalize=True,
                      title='Validation confusion matrix')
        plt.figure()
        plot_confusion_matrix(full_confusions[2], classes=POSSIBLE_LABELS, normalize=True,
                      title='Test sample confusion matrix')

        plt.show()
