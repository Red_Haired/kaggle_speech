import tensorflow as tf
from tensorflow.contrib import layers
import numpy as np
from transform import spectrogram


def rnn(wav, params, is_training):
    mean, var = tf.nn.moments(wav, axes=[1])
    cond = tf.greater(var, 0.0)
    wav = tf.where(cond, tf.transpose(tf.transpose(wav) / var), wav)

    x = spectrogram(wav, name='spectrogram')
    keep_prob = tf.cond(is_training, lambda: params['keep_prob'], lambda: 1.0)
    x = layers.batch_norm(x, is_training=is_training)
    for i in range(3):
        x = layers.conv2d(
            x, 16 * (2 ** i), 3, (2, 1),
            activation_fn=tf.nn.elu,
            normalizer_fn=layers.batch_norm if params['use_batch_norm'] else None,
            normalizer_params={'is_training': is_training}
        )
        #x = tf.nn.dropout(x, keep_prob=keep_prob)
        #x = layers.max_pool2d(x, 2, 2)

    shape = x.get_shape().as_list()  # a list: [None, 9, 2]
    dim = np.prod(shape[2:])
    x = tf.reshape(x, [tf.shape(x)[0], tf.shape(x)[1], dim])

    lstm_cell_fw = tf.contrib.rnn.BasicLSTMCell(256, forget_bias=1.0)
    lstm_cell_bw = tf.contrib.rnn.BasicLSTMCell(256, forget_bias=1.0)

    outputs, output_states = tf.nn.bidirectional_dynamic_rnn(
        cell_fw=lstm_cell_fw,
        cell_bw=lstm_cell_bw,
        # Input is the previous Fully Connected Layer before the LSTM
        inputs=x,
        dtype=tf.float32,
        time_major=False)
    x = tf.concat((outputs[0][:, -1, :], outputs[1][:, 0, :]), 1)
    x = tf.nn.dropout(x, keep_prob=keep_prob)
    var = tf.log(var + 1e-6)
    var = tf.expand_dims(var, 1)
    var = layers.batch_norm(var, is_training=is_training)
    x = tf.concat([x, var], 1)
    x = tf.expand_dims(x, 1)
    # we can use conv2d 1x1 instead of dense
    x = tf.layers.conv1d(x, 128, 1, 1, activation=tf.nn.elu)
    x = tf.nn.dropout(x, keep_prob=keep_prob)

    # again conv2d 1x1 instead of dense layer
    logits = tf.layers.conv1d(x, params['num_classes'], 1, 1, activation=None)
    return tf.squeeze(logits, [1])
