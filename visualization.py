import numpy as np
import matplotlib.pyplot as plt


def plot_wave(ax, name, samples, sample_rate=16000):
    ax.set_title('Raw wave of ' + name)
    ax.set_ylabel('Amplitude')
    ax.plot(np.linspace(0, sample_rate / len(samples), sample_rate), samples)


def plot_spectrogram(ax, name, spectrogram):
    ax.imshow(spectrogram[:,:,0].T, aspect='auto', origin='lower')
    ax.set_title('Spectrogram of ' + name)
    ax.set_ylabel('Freqs in Hz')
    ax.set_xlabel('Seconds')


def plot_sound(name, samples, spectrogram):
    fig = plt.figure(figsize=(14, 4))
    ax1 = fig.add_subplot(1, 2, 1)
    ax2 = fig.add_subplot(1, 2, 2)
    plot_wave(ax1, name, samples)
    plot_spectrogram(ax2, name, spectrogram)