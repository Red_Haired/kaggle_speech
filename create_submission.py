from tqdm import tqdm
import os
from glob import glob
import argparse
import tensorflow as tf
import numpy as np
from datagenerator import id2name, DataGenerator
from manual_train import OUTDIR, DATADIR, params, ensemble_prediction


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_dir', type=str, default=OUTDIR+'rnn')
    parser.add_argument('--iteration', default=None)
    parser.add_argument('--ensemble', action='store_true')
    parser.add_argument('--batch_size', type=int, default=params['batch_size'])
    args = parser.parse_args()
    params['batch_size'] = args.batch_size

    model_meta_path = tf.train.latest_checkpoint(args.model_dir) + '.meta'
    saver = tf.train.import_meta_graph(model_meta_path)
    graph = tf.get_default_graph()
    prediction = graph.get_tensor_by_name('prediction:0')
    wav = graph.get_tensor_by_name('wav:0')
    labels = graph.get_tensor_by_name('labels:0')
    logits = graph.get_tensor_by_name('logits:0')
    n = 1
    if args.ensemble:
        prediction, accuracy, labels = ensemble_prediction(labels, logits)
        n = 2
    fnames = []
    labels = []
    paths = glob(os.path.join(DATADIR, 'test/audio/*wav'))
    test_data_generator = DataGenerator(paths).get_generator(include_label=False, include_filename=True, repeate_sample=args.ensemble)

    batch_size = params['batch_size']*n
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True

    with tf.Session(config=config) as sess:
        # To initialize values with saved data
        if not args.iteration:
            chekpoint_filepath = tf.train.latest_checkpoint(args.model_dir)
            print(chekpoint_filepath)
        else:
            name = 'model-%d' % int(args.iteration)
            chekpoint_filepath = os.path.join(args.model_dir, name)
        saver.restore(sess, chekpoint_filepath)
        wav_batch = []
        for fname, wav_val in tqdm(test_data_generator()):
            wav_batch.append(wav_val)
            fnames.append(fname)
            if len(wav_batch) == batch_size:
                res = sess.run(prediction, feed_dict={wav: wav_batch})
                for l in res:
                    labels.append(l)
                wav_batch = []
        if len(wav_batch) > 0:
            res = sess.run(prediction, feed_dict={wav: wav_batch})
            for l in res:
                labels.append(l)

    if args.ensemble:
        fnames = np.reshape(fnames, (-1, 2))[:, 0]
    # last batch will contain padding, so remove duplicates
    submission = dict()
    for fname, label in tqdm(zip(fnames, labels)):
        submission[fname] = id2name[label]

    with open(os.path.join(args.model_dir, 'submission.csv'), 'w') as fout:
        fout.write('fname,label\n')
        for fname, label in submission.items():
            fout.write('{},{}\n'.format(fname, label))
